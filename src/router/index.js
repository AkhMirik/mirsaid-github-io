import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import contact from '../views/contact-tab'
import resume from '../views/resume-tab'
import skills from '../views/skills-tab'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/contact',
        name: 'contact',
        component: contact
    },
    {
        path: '/resume',
        name: 'resume',
        component: resume
    },
    {
        path: '/skills',
        name: 'skills',
        component: skills
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
